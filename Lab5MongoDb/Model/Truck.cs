﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Model
{
    class Truck
    {
        [BsonId]
        public Guid id { get; set; }
        public string name { get; set; }

        public Guid trailer_Id { get; set; }

        public Truck()
        {
        }

        public Truck(string name, Guid trailer_Id)
        {
            this.name = name;
            this.trailer_Id = trailer_Id;
        }

        public override string ToString()
        {
            return "Ciezarowka - Id: " + id + " Nazwa: " + name + (trailer_Id == Guid.Empty? " Brak przypisanej naczepy " : " Id naczepy: " + trailer_Id);
        }
    }
}
