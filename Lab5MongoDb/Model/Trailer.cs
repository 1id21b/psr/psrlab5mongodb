﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Model
{
    class Trailer
    {
        [BsonId]
        public Guid id { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return "Przyczepa - Id: " + id + " Nazwa: " + name; 
        }

        public Trailer()
        {
        }

        public Trailer(string name)
        {
            this.name = name;
        }
    }
}
