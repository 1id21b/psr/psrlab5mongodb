﻿using Lab5MongoDb.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class DriverRep
    {

        IMongoDatabase db;
        public DriverRep(IMongoDatabase _db)
        {
            db = _db;
        }

        public void AddDriver(Driver _d)
        {
            var col = db.GetCollection<Driver>("Driver");
            col.InsertOne(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteDriver(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            FilterDefinition<Driver> filter = Builders<Driver>.Filter.Eq("id", id);
            try
            {
                col.FindOneAndDelete(filter);
                Console.WriteLine("Usunięto ciezarowke o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiej ciezarowki");
            }
        }
        public void EditDriver(string id, string name, string surname, string pesel, string age)
        {
            var col = db.GetCollection<Driver>("Driver");
            FilterDefinition<Driver> filter = Builders<Driver>.Filter.Eq("id", id);
            UpdateDefinition<Driver> update;
            Driver _t = new Driver();
            try
            {
                update = Builders<Driver>.Update.Set("name", name);
                col.UpdateOne(filter, update);
                update = Builders<Driver>.Update.Set("surname", surname);
                col.UpdateOne(filter, update);
                update = Builders<Driver>.Update.Set("pesel", pesel);
                col.UpdateOne(filter, update);
                update = Builders<Driver>.Update.Set("age", int.Parse(age));
                col.UpdateOne(filter, update);
                _t = col.Find(filter).First();
                Console.WriteLine("Zedytowano ciezarowke o Id: " + id);
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }
        public void ShowDrivers()
        {
            var col = db.GetCollection<Driver>("Driver");
            var Drivers = col.Find(new BsonDocument()).ToList();
            foreach (Driver t in Drivers)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowDriver(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            FilterDefinition<Driver> filter = Builders<Driver>.Filter.Eq("id", id);
            Driver _t = new Driver();
            try
            {
                _t = col.Find(filter).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }
    }
}
