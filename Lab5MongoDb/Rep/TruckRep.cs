﻿using Lab5MongoDb.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class TruckRep
    {
        IMongoDatabase db;
        public TruckRep(IMongoDatabase _db)
        {
            db = _db;
        }

        public void AddTruck(Truck _d)
        {
            var col = db.GetCollection<Truck>("Truck");
            col.InsertOne(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteTruck(string id)
        {
            var col = db.GetCollection<Truck>("Truck");
            FilterDefinition<Truck> filter = Builders<Truck>.Filter.Eq("id", id);
            try
            {
                col.FindOneAndDelete(filter);
                Console.WriteLine("Usunięto ciezarowke o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiej ciezarowki");
            }
        }
        public void EditTruck(string id, string name)
        {
            var col = db.GetCollection<Truck>("Truck");
            FilterDefinition<Truck> filter = Builders<Truck>.Filter.Eq("id", id);
            UpdateDefinition<Truck> update;
            update = Builders<Truck>.Update.Set("name", name);
            Truck _t = new Truck();
            try
            {
                col.UpdateOne(filter, update);
                _t = col.Find(filter).First();
                Console.WriteLine("Zedytowano ciezarowke o Id: " + id);
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }
        public void ShowTrucks()
        {
            var col = db.GetCollection<Truck>("Truck");
            var Trucks = col.Find(new BsonDocument()).ToList();
            foreach (Truck t in Trucks)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowTruck(string id)
        {
            var col = db.GetCollection<Truck>("Truck");
            FilterDefinition<Truck> filter = Builders<Truck>.Filter.Eq("id", id);
            Truck _t = new Truck();
            try
            {
                _t = col.Find(filter).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma ciezarowki z takim id");
            }
        }

        public void ShowDriversOfTruck(string id)
        {
            var col = db.GetCollection<Driver>("Driver");
            FilterDefinition<Driver> filter = Builders<Driver>.Filter.Eq("truck_Id", id);
            var drivers = col.Find(filter).ToList();
            if (drivers.Count > 0)
                Console.WriteLine("Do ciężrówki o id: " + id + " przypisani są:");
            else
                Console.WriteLine("Bledne id ciezarowki albo nie ma zadnych kierowcow przypisanych");
            foreach (Driver d in drivers)
            {
                Console.WriteLine(d);
            }
        }
    }
}
