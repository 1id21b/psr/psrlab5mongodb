﻿using Lab5MongoDb.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5MongoDb.Rep
{
    class TrailerRep
    {
        private IMongoDatabase db { get; set; }
        public TrailerRep(IMongoDatabase _db)
        {
            db = _db;
        }

        public void AddTrailer(Trailer _d)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            col.InsertOne(_d);
            Console.WriteLine("Dodano: " + _d);
        }
        public void DeleteTrailer(string id)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            FilterDefinition<Trailer> filter = Builders<Trailer>.Filter.Eq("id", id);
            try
            {
                col.FindOneAndDelete(filter);
                Console.WriteLine("Usunięto przyczepe o Id: " + id);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma takiej przyczepy");
            }
        }
        public void EditTrailer(string id, string name)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            FilterDefinition<Trailer> filter = Builders<Trailer>.Filter.Eq("id", id);
            UpdateDefinition<Trailer> update;
            update = Builders<Trailer>.Update.Set("name", name);
            Trailer _t = new Trailer();
            try
            {
                col.UpdateOne(filter, update);
                _t = col.Find(filter).First();
                Console.WriteLine("Zedytowano przyczepe o Id: " + id);
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma przyczepy z takim id");
            }
        }
        public void ShowTrailers()
        {
            var col = db.GetCollection<Trailer>("Trailer");
            var trailers = col.Find(new BsonDocument()).ToList();
            foreach(Trailer t in trailers)
            {
                Console.WriteLine(t);
            }
        }
        public void ShowTrailer(string id)
        {
            var col = db.GetCollection<Trailer>("Trailer");
            FilterDefinition<Trailer> filter = Builders<Trailer>.Filter.Eq("id", id);
            Trailer _t = new Trailer();
            try
            {
                _t = col.Find(filter).First();
                Console.WriteLine(_t);
            }
            catch (Exception)
            {
                Console.WriteLine("Nie ma przyczepy z takim id");
            }
        }

    }
}
